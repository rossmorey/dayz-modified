import React from "react";
import { render } from "react-dom";
import moment from "./src/moment-range";
import Dayz from "./src/dayz";
require("./demo.scss");
let COUNT = 1;

class DayzTestComponent extends React.Component {
  constructor(props) {
    super(props);
    this.addEvent = this.addEvent.bind(this);
    this.onEventClick = this.onEventClick.bind(this);
    this.editComponent = this.editComponent.bind(this);
    this.changeDisplay = this.changeDisplay.bind(this);
    this.onEventResize = this.onEventResize.bind(this);
    const date = moment("2023-04-21");
    this.state = {
      date,
      display: "day",
      events: new Dayz.EventsCollection([
        {
          content: "Continuing event Past",
          range: moment.range(moment("2015-09-08"), moment("2015-09-14")),
        },

        {
          content: "Continuing event Before",
          range: moment.range("2015-09-04", "2015-09-09"),
        },

        {
          content: "Weeklong",
          range: moment.range("2023-04-14", moment("2023-04-21").endOf("day")),
        },

        {
          content: "A Longer Event",
          range: moment.range(moment("2023-04-11"), moment("2023-04-21")),
        },

        {
          content: "Inclusive",
          range: moment.range(moment("2023-04-07"), moment("2023-04-12")),
        },
        {
          content: "2 - 3 am",
          resizable: { step: 15 },

          range: moment.range(
            moment("2023-04-21").hour(2),
            moment("2023-04-21").hour(3)
          ),
        },
        {
          content: "8am - 8pm (non-resizable)",
          id: 3,
          overlapIds: [1, 2, 3],
          range: moment.range(
            moment("2023-04-21").hour(8),
            moment("2023-04-21").hour(20)
          ),
        },
        {
          content: "7am - 2pm (resizable)",
          id: 1,
          resizable: { step: 15 },
          overlapIds: [1, 2],
          range: moment.range(
            moment("2023-04-21").hour(7),
            moment("2023-04-21").hour(14)
          ),
        },
        {
          content: "9am - 2pm (resizable)",
          id: 2,
          resizable: { step: 15 },
          overlapIds: [2, 3],
          range: moment.range(
            moment("2023-04-21").hour(9),
            moment("2023-04-21").hour(14)
          ),
        },
        {
          id: 5,
          content: "4 - 5 am",
          resizable: { step: 15 },
          overlapIds: [],
          range: moment.range(
            moment("2023-04-21").hour(4),
            moment("2023-04-21").hour(5)
          ),
        },
      ]),
    };
  }

  changeDisplay(ev) {
    this.setState({ display: ev.target.value });
  }

  onEventClick(ev, event) {
    event.set({ editing: !event.isEditing() });
  }
  onEventResize(ev, event) {
    const start = event.start().format("hh:mma");
    const end = event.end().format("hh:mma");
    event.set({ content: `${start} - ${end} (resizable)` });
  }
  addEvent(ev, date) {
    this.state.events.add({
      content: `Event ${COUNT++}`,
      resizable: true,
      range: moment.range(
        date.clone(),
        date.clone().add(1, "hour").add(45, "minutes")
      ),
    });
  }

  editComponent(props) {
    const onBlur = function () {
      props.event.set({ editing: false });
    };
    const onChange = function (ev) {
      props.event.set({ content: ev.target.value });
    };
    const onDelete = function () {
      props.event.remove();
    };
    return (
      <div className="edit">
        <input
          type="text"
          autoFocus
          value={props.event.content()}
          onChange={onChange}
          onBlur={onBlur}
        />
        <button onClick={onDelete}>X</button>
      </div>
    );
  }

  render() {
    return (
      <div className="dayz-test-wrapper">
        <div className="tools">
          <label>
            Month:{" "}
            <input
              type="radio"
              name="style"
              value="month"
              onChange={this.changeDisplay}
              checked={"month" === this.state.display}
            />
          </label>
          <label>
            Week:{" "}
            <input
              type="radio"
              name="style"
              value="week"
              onChange={this.changeDisplay}
              checked={"week" === this.state.display}
            />
          </label>
          <label>
            Day:{" "}
            <input
              type="radio"
              name="style"
              value="day"
              onChange={this.changeDisplay}
              checked={"day" === this.state.display}
            />
          </label>
        </div>

        <Dayz
          {...this.state}
          onEventResize={this.onEventResize}
          editComponent={this.editComponent}
          onDayDoubleClick={this.addEvent}
          onEventClick={this.onEventClick}
        ></Dayz>
      </div>
    );
  }
}

const div = document.createElement("div");
document.body.appendChild(div);
render(React.createElement(DayzTestComponent, {}), div);
